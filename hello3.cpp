
///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab07c - My First Cat - EE 205 - Spr 2022
///
/// @file    hello3.c
/// @version 1.0 - Initial version
///
/// "hello3.cpp" is a file written for us to define a Cat class and call upon the
/// method of the cat class which is sayHello.
///
///
/// @author  Kobe Uyeda <kobek@hawaii.edu>
/// @date    01/MAR/2022
///
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

using namespace std;

class Cat {
   public:
      void sayHello(){
         cout << "Meow" << endl;
      }
};


int main() {
   Cat myCat;
   myCat.sayHello();
   return 0;
}

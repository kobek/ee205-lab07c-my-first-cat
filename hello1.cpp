///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab07c - My First Cat - EE 205 - Spr 2022
///
/// @file    hello1.c
/// @version 1.0 - Initial version
/// 
/// "hello1.cpp" is a hello wold file written in c++ using namespace to define std
///
///
/// @author  Kobe Uyeda <kobek@hawaii.edu>
/// @date    01/MAR/2022
///
///////////////////////////////////////////////////////////////////////////////


#include <iostream>

using namespace std;

int main() {
   cout << "Hello World" << endl;
   return 0;
}
